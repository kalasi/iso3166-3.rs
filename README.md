[![ci-badge][]][ci] [![license-badge][]][license] [![docs-badge][]][docs]

# iso3166-3.rs

Rust crate for ISO 3166-3 data.

[Documentation](http://docs.austinhellyer.me/iso3166_3)

### What is ISO 3166-3?

> ISO 3166-3 is part of the ISO 3166 standard published by the International
> Organization for Standardization (ISO), and defines codes for country names
> which have been deleted from ISO 3166-1 since its first publication in 1974.
>
> -- [Wikipedia](https://en.wikipedia.org/wiki/ISO_3166-3)

### Installation

Add the following dependency to your Cargo.toml:

```rust
iso3166_3 = "0.3"
```

And include it in your project:

```rust
extern crate iso3166_3;
```

### Examples

Retrieve all former country codes:

```rust
let countries = iso3166_3::all().unwrap();
```

Retrieve a former country code by its ISO 3166-3 four-character code:

```rust
let country = iso3166_3::code("ZRCD").unwrap();
```

Retrieve a vector of former country codes by a range of when the code was
valid:

```rust
// Codes valid from years 1974-1990.
iso3166_3::validity(Some(1974), Some(1990));

// Codes valid from years 1990 onward to infinity:
iso3166_3::validity(Some(1990), None);

// Codes valid until 1998 and prior:
iso3166_3::validity(None, Some(1998));
```

### License

License info in [LICENSE.md]. Long story short, ISC.

[ci]: https://gitlab.com/kalasi/iso3166-3.rs/pipelines
[ci-badge]: https://gitlab.com/kalasi/iso3166-3.rs/badges/master/build.svg
[docs]: https://docs.austinhellyer.me/iso3166-3/
[docs-badge]: https://img.shields.io/badge/docs-online-2020ff.svg
[license]: https://opensource.org/licenses/ISC
[license-badge]: https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square
[LICENSE.md]: https://gitlab.com/kalasi/iso3166-3.rs/blob/master/LICENSE.md
