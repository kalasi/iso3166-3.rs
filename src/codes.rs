// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//
// What is ISO 3316-3?
// | ISO 3166-3 is part of the ISO 3166 standard published by the International
// | Organization for Standardization (ISO), and defines codes for country
// | names which have been deleted from ISO 3166-1 since its first publication
// | in 1974.
// |
// | - [Wikipedia](http://en.wikipedia.org/wiki/ISO_3316-3)
//
// Originally by zeyla on GitHub.

use {FormerCountryCodeCodes, FormerCountryCode};

/// Returns `Vec` of all `FormerCountryCode`s defined by ISO 3166-3.
///
/// # Examples
///
/// ```
/// let _countries = iso3166_3::all();
/// ```
pub fn former_country_codes<'a>() -> Vec<FormerCountryCode<'a>> {
    let mut codes = vec![];

    // Begin
    codes.push(FormerCountryCode {
        code: "BQAQ",
        codes_former: FormerCountryCodeCodes {
            alpha2: "BQ",
            alpha3: "ATB",
            num: "080",
        },
        description: "Merged into Antarctica",
        name: "British Antarctic Territory",
        validity: [1974, 1979],
    });
    codes.push(FormerCountryCode {
        code: "BUMM",
        codes_former: FormerCountryCodeCodes {
            alpha2: "BU",
            alpha3: "BUR",
            num: "104",
        },
        description: "Name changed to Myanmar",
        name: "Burma",
        validity: [1974, 1989],
    });
    codes.push(FormerCountryCode {
        code: "BYAA",
        codes_former: FormerCountryCodeCodes {
            alpha2: "BY",
            alpha3: "BYS",
            num: "112",
        },
        description: "Name changed to Belarus",
        name: "Byelorussian SSR",
        validity: [1974, 1992],
    });
    codes.push(FormerCountryCode {
        code: "CTKI",
        codes_former: FormerCountryCodeCodes {
            alpha2: "CT",
            alpha3: "CTE",
            num: "128",
        },
        description: "Merged into Kiribati",
        name: "Canton and Enderbury Islands",
        validity: [1974, 1984],
    });
    codes.push(FormerCountryCode {
        code: "CSHH",
        codes_former: FormerCountryCodeCodes {
            alpha2: "CS",
            alpha3: "CSK",
            num: "200",
        },
        description: "Divided into: Czech Republic; Slovakia",
        name: "Czechoslovakia",
        validity: [1974, 1993],
    });
    codes.push(FormerCountryCode {
        code: "DYBJ",
        codes_former: FormerCountryCodeCodes {
            alpha2: "DY",
            alpha3: "DHY",
            num: "204",
        },
        description: "Name changed to Benin",
        name: "Dahomey",
        validity: [1974, 1977],
    });
    codes.push(FormerCountryCode {
        code: "NQAQ",
        codes_former: FormerCountryCodeCodes {
            alpha2: "NQ",
            alpha3: "ATN",
            num: "216",
        },
        description: "Merged into Antarctica",
        name: "Dronning Maud Land",
        validity: [1974, 1983],
    });
    codes.push(FormerCountryCode {
        code: "TPTL",
        codes_former: FormerCountryCodeCodes {
            alpha2: "TP",
            alpha3: "TMP",
            num: "626",
        },
        description: "Name changed to Timor-Leste",
        name: "East Timor",
        validity: [1974, 2002],
    });
    codes.push(FormerCountryCode {
        code: "FXFR",
        codes_former: FormerCountryCodeCodes {
            alpha2: "FX",
            alpha3: "FXX",
            num: "249",
        },
        description: "Merged into France",
        name: "France, Metropolitan",
        validity: [1993, 1997],
    });
    codes.push(FormerCountryCode {
        code: "AIDJ",
        codes_former: FormerCountryCodeCodes {
            alpha2: "AI",
            alpha3: "AFI",
            num: "262",
        },
        description: "Name changed to Djibouti",
        name: "French Afar and Issas",
        validity: [1974, 1977],
    });
    codes.push(FormerCountryCode {
        code: "FQHH",
        codes_former: FormerCountryCodeCodes {
            alpha2: "FQ",
            alpha3: "ATF",
            num: "260",
        },
        description: "Divided into: Part of Antarctica; French Southern Territories",
        name: "French Southern and Antarctic Territories",
        validity: [1974, 1979],
    });
    codes.push(FormerCountryCode {
        code: "DDDE",
        codes_former: FormerCountryCodeCodes {
            alpha2: "DD",
            alpha3: "DDR",
            num: "278",
        },
        description: "Merged into Germany",
        name: "German Democratic Republic",
        validity: [1974, 1990],
    });
    codes.push(FormerCountryCode {
        code: "GEHH",
        codes_former: FormerCountryCodeCodes {
            alpha2: "GE",
            alpha3: "GEL",
            num: "296",
        },
        description: "Divided into: Kiribati; Tuvalu",
        name: "Gilbert and Ellice Islands",
        validity: [1974, 1979],
    });
    codes.push(FormerCountryCode {
        code: "JTUM",
        codes_former: FormerCountryCodeCodes {
            alpha2: "JT",
            alpha3: "JTN",
            num: "396",
        },
        description: "Merged into United States Minor Outlying Islands",
        name: "Johnston Island",
        validity: [1974, 1986],
    });
    codes.push(FormerCountryCode {
        code: "MIUM",
        codes_former: FormerCountryCodeCodes {
            alpha2: "MI",
            alpha3: "MID",
            num: "488",
        },
        description: "Merged into United States Minor Outlying Islands",
        name: "Midway Islands",
        validity: [1974, 1986],
    });
    codes.push(FormerCountryCode {
        code: "ANHH",
        codes_former: FormerCountryCodeCodes {
            alpha2: "AN",
            alpha3: "ANT",
            num: "530",
        },
        description: "Divided into: Bonaire, Sint Eustatius and Saba; Curaçao; Sint Maarten",
        name: "Netherlands Antilles",
        validity: [1974, 2010],
    });
    codes.push(FormerCountryCode {
        code: "NTHH",
        codes_former: FormerCountryCodeCodes {
            alpha2: "NT",
            alpha3: "NTZ",
            num: "536",
        },
        description: "Divided into: Part of Iraq; Part of Saudi Arabia",
        name: "Neutral Zone",
        validity: [1974, 1993],
    });
    codes.push(FormerCountryCode {
        code: "NHVU",
        codes_former: FormerCountryCodeCodes {
            alpha2: "NH",
            alpha3: "NHB",
            num: "548",
        },
        description: "Name changed to Vanuatu",
        name: "New Hebrides",
        validity: [1974, 1980],
    });
    codes.push(FormerCountryCode {
        code: "PCHH",
        codes_former: FormerCountryCodeCodes {
            alpha2: "PC",
            alpha3: "PCI",
            num: "582",
        },
        description: "Divided into: Marshall Islands; Micronesia, Federated States of; Northern Mariana Islands; Palau",
        name: "Pacific Islands, Trust Territory of the",
        validity: [1974, 1986],
    });
    codes.push(FormerCountryCode {
        code: "PZPA",
        codes_former: FormerCountryCodeCodes {
            alpha2: "PZ",
            alpha3: "PCZ",
            num: "594",
        },
        description: "Merged into Panama",
        name: "Panama Canal Zone",
        validity: [1974, 1980],
    });
    codes.push(FormerCountryCode {
        code: "CSXX",
        codes_former: FormerCountryCodeCodes {
            alpha2: "CS",
            alpha3: "SCG",
            num: "891",
        },
        description: "Divided into: Montenegro; Serbia",
        name: "Serbia and Montenegro",
        validity: [2003, 2006],
    });
    codes.push(FormerCountryCode {
        code: "SKIN",
        codes_former: FormerCountryCodeCodes {
            alpha2: "SK",
            alpha3: "SKM",
            num: "698",
        },
        description: "Merged into India",
        name: "Sikkim",
        validity: [1974, 1975],
    });
    codes.push(FormerCountryCode {
        code: "RHZW",
        codes_former: FormerCountryCodeCodes {
            alpha2: "RH",
            alpha3: "RHO",
            num: "716",
        },
        description: "Name changed to Zimbabwe",
        name: "Southern Rhodesia",
        validity: [1974, 1980],
    });
    codes.push(FormerCountryCode {
        code: "HVBF",
        codes_former: FormerCountryCodeCodes {
            alpha2: "HV",
            alpha3: "HVO",
            num: "854",
        },
        description: "Name changed to Burkina Faso",
        name: "Upper Volta",
        validity: [1974, 1984],
    });
    codes.push(FormerCountryCode {
        code: "PUUM",
        codes_former: FormerCountryCodeCodes {
            alpha2: "PU",
            alpha3: "PUS",
            num: "849",
        },
        description: "Merged into United States Minor Outlying Islands",
        name: "Us Miscellaneous Pacific Islands",
        validity: [1974, 1986],
    });
    codes.push(FormerCountryCode {
        code: "SUHH",
        codes_former: FormerCountryCodeCodes {
            alpha2: "SU",
            alpha3: "SUN",
            num: "810",
        },
        description: "Divided into: Armenia; Azerbaijan; Estonia; Georgia; Kazakhstan; Kyrgyzstan; Latvia; Lithuania; Moldova, Republic of; Russian Federation; Tajikistan; Turkmenistan; Uzbekistan",
        name: "Ussr",
        validity: [1974, 1992],
    });
    codes.push(FormerCountryCode {
        code: "VDVN",
        codes_former: FormerCountryCodeCodes {
            alpha2: "VD",
            alpha3: "VDR",
            num: "704",
        },
        description: "Merged into Viet Nam",
        name: "Viet-Nam, Democratic Republic of",
        validity: [1974, 1977],
    });
    codes.push(FormerCountryCode {
        code: "WKUM",
        codes_former: FormerCountryCodeCodes {
            alpha2: "WK",
            alpha3: "WAK",
            num: "872",
        },
        description: "Merged into United States Minor Outlying Islands",
        name: "Wake Island",
        validity: [1974, 1986],
    });
    codes.push(FormerCountryCode {
        code: "YDYE",
        codes_former: FormerCountryCodeCodes {
            alpha2: "YD",
            alpha3: "YMD",
            num: "720",
        },
        description: "Merged into Yemen",
        name: "Yemen, Democratic",
        validity: [1974, 1990],
    });
    codes.push(FormerCountryCode {
        code: "YUCS",
        codes_former: FormerCountryCodeCodes {
            alpha2: "YU",
            alpha3: "YUG",
            num: "891",
        },
        description: "Name changed to Serbia and Montenegro",
        name: "Yugoslavia",
        validity: [1974, 2003],
    });
    codes.push(FormerCountryCode {
        code: "ZRCD",
        codes_former: FormerCountryCodeCodes {
            alpha2: "ZR",
            alpha3: "ZAR",
            num: "180",
        },
        description: "Name changed to Congo, the Democratic Republic of the",
        name: "Zaire",
        validity: [1974, 1997],
    });
    // End

    codes
}
