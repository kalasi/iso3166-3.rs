// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//
// What is ISO 3166-3?
// | ISO 3166-3 is part of the ISO 3166 standard published by the International
// | Organization for Standardization (ISO), and defines codes for country
// | names which have been deleted from ISO 3166-1 since its first publication
// | in 1974.
// |
// | - [Wikipedia](http://en.wikipedia.org/wiki/ISO_3166-3)
//
// Originally by zeyla on GitHub.

mod codes;

pub use codes::former_country_codes as all;

/// A representation of a Former Country Code's ISO 3166-1 information.
#[derive(Clone, Copy, Debug)]
pub struct FormerCountryCodeCodes<'a> {
    /// Two-character code for the country code
    alpha2: &'a str,
    /// Three-character code for the country code
    alpha3: &'a str,
    /// Three digit code assigned to the coutnry code
    num: &'a str,
}

/// Struct defining a Former Country Code as defined by ISO 3166-3.
#[derive(Clone, Copy, Debug)]
pub struct FormerCountryCode<'a> {
    /// Four-letter code assigned for the former country name
    pub code: &'a str,
    /// The former codes that this country had
    pub codes_former: FormerCountryCodeCodes<'a>,
    /// Reason why the code was deprecated (e.g.: country merge, divided)
    pub description: &'a str,
    /// The former country's name
    pub name: &'a str,
    /// The period in years of which this code was valid
    pub validity: [i16; 2],
}

/// Returns the `FormerCountryCode` if one is found matching the given code.
///
/// # Examples
///
/// ```
/// let _country = iso3166_3::code("DDDE");
/// ```
pub fn code(code_given: &str) -> Option<FormerCountryCode> {
    all()
        .iter()
        .find(|code| code.code == code_given)
        .map(|code| *code)
}

/// Returns a `Vec` of ISO 3166-3 `FormerCountryCode`s that were valid from a
/// range of the given years.
///
/// # Examples
///
/// Retrieve codes valid from years 1974-1990:
///
/// ```
/// let _countries = iso3166_3::validity(Some(1974), Some(1990));
/// ```
///
/// Retrieve codes valid from the year 1990 onward:
///
/// ```
/// let _countries = iso3166_3::validity(Some(1990), None);
/// ```
///
/// Retrieve codes valid until 1998 and prior:
///
/// ```
/// let _countries = iso3166_3::validity(None, Some(1998));
/// ```
pub fn validity<'a>(from: Option<i16>,
                    to: Option<i16>) -> Vec<FormerCountryCode<'a>> {
    let do_from = from.is_some();
    let do_to = to.is_some();
    let val_from = from.unwrap_or(0);
    let val_to = to.unwrap_or(0);

    all().iter()
        .filter(|code| {
            if do_from && do_to {
                code.validity[0] >= val_from && code.validity[1] <= val_to
            } else if do_from {
                code.validity[0] >= val_from
            } else if do_to {
                code.validity[1] <= val_to
            } else {
                false
            }
        })
        .map(|code| *code)
        .collect()
}
