# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

### Added

### Changed

## [0.3.0] - 2016-08-21

### Changed

- Simplified many operations;
- Instead of wrapping Vec returns in an Option, Vec's are directly returned.

### Added

## [0.2.0] - 2016-01-13

### Changed
- "British Antarctica Territory" -> "British Antarctic Territory".
- Removed an empty entry.
- "U.S. Miscellaneous Pacific Islands" -> "Us Miscellaneous Pacific Islands".
- "USSR" -> "Ussr".
- "Viet-Nam, Democratic Republic Of" -> "Viet-Nam, Democratic Republic of".

### Added
- Python script to auto-update ISO 3166-3 codes.

## [0.1.0] - 2016-01-11

Initial commit.

[Unreleased]: https://gitlab.com/kalasi/iso3166-3.rs/compare/v0.2.0...master
[0.3.0]: https://gitlab.com/kalasi/iso3166-3.rs/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/kalasi/iso3166-3.rs/compare/v0.1.0...v0.2.0
