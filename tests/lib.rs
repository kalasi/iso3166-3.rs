// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//
// What is ISO 3166-3?
// | ISO 3166-3 is part of the ISO 3166 standard published by the International
// | Organization for Standardization (ISO), and defines codes for country
// | names which have been deleted from ISO 3166-1 since its first publication
// | in 1974.
// |
// | - [Wikipedia](http://en.wikipedia.org/wiki/ISO_3166-3)
//
// Originally by zeyla on GitHub.

extern crate iso3166_3;

use iso3166_3::{all, code, validity};

// Mass entries of version bumps due to BC breaks:
// 001: Modified total number of entries from 32 to 31 and modified some
//      descriptions and names, causing a BC break. Version 0.1 -> 0.2.
#[test]
fn get_all() {
    assert!(all().len() > 0);
}

#[test]
fn code_tests() {
    // Test a code that exists.
    assert!(code("DDDE").is_some());

    // Test a code that does not exist.
    assert!(code("XXXX").is_none());
}

#[test]
fn validaty_variations() {
    assert!(validity(Some(1993), Some(1997)).len() == 1);
    assert!(validity(Some(1980), Some(1990)).len() == 0);

    assert!(validity(None, Some(1980)).len() == 10);
    assert!(validity(Some(1993), None).len() == 2);
    assert!(validity(None, None).len() == 0);
}

#[test]
fn validity_random_years() {
    assert!(validity(Some(1974), Some(1977)).len() == 4);
}

#[test]
fn backwards_compat() {
    assert!(all().len() == 31);
    assert!(validity(Some(1974), None).len() == 31);
    assert!(validity(Some(1993), Some(1994)).len() == 0);
    assert!(validity(Some(1974), None).len() == 31);
}
